<?php

use yii\helpers\Html;
use yii\grid\GridView;
use begenk\auth\components\RouteRule;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel begenk\auth\models\searchs\AuthItem */
/* @var $context begenk\auth\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('begenk-auth', $labels['Items']);
$this->params['breadcrumbs'][] = $this->title;

$rules = array_keys(Yii::$app->getAuthManager()->getRules());
$rules = array_combine($rules, $rules);
unset($rules[RouteRule::RULE_NAME]);
?>
<div class="role-index">
    <p>
        <?= Html::a(Yii::t('begenk-auth', 'Create ' . $labels['Item']), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('begenk-auth', 'Name'),
            ],
            [
                'attribute' => 'ruleName',
                'label' => Yii::t('begenk-auth', 'Rule Name'),
                'filter' => $rules
            ],
            [
                'attribute' => 'description',
                'label' => Yii::t('begenk-auth', 'Description'),
            ],
            ['class' => 'yii\grid\ActionColumn',],
        ],
    ])
    ?>

</div>
