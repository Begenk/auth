<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model begenk\auth\models\AuthItem */
/* @var $context begenk\auth\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('begenk-auth', 'Update ' . $labels['Item']) . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('begenk-auth', $labels['Items']), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('begenk-auth', 'Update');
?>
<div class="auth-item-update">
    <?=
    $this->render('_form', [
        'model' => $model,
    ]);
    ?>
</div>
