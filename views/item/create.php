<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model begenk\auth\models\AuthItem */
/* @var $context begenk\auth\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('begenk-auth', 'Create ' . $labels['Item']);
$this->params['breadcrumbs'][] = ['label' => Yii::t('begenk-auth', $labels['Items']), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">
    <?=
    $this->render('_form', [
        'model' => $model,
    ]);
    ?>

</div>
