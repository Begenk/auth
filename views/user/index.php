<?php

use yii\helpers\Html;
use yii\grid\GridView;
use begenk\auth\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel begenk\auth\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('begenk-auth', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <p>
        <?= Html::a(Yii::t('begenk-auth', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'full_name',
            'email:email',
            'created_at:date',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->status == 0 ? 'Inactive' : 'Active';
                },
                'filter' => [
                    0 => 'Inactive',
                    10 => 'Active'
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Helper::filterActionColumn(['view', 'activate', 'deactivate', 'delete']),
                'buttons' => [
                    'activate' => function($url, $model) {
                        if ($model->status == 10) {
                            return '';
                        }
                        $options = [
                            'title' => Yii::t('begenk-auth', 'Activate'),
                            'aria-label' => Yii::t('begenk-auth', 'Activate'),
                            'data-confirm' => Yii::t('begenk-auth', 'Are you sure you want to activate this user?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="bi bi-check2-circle"></span>', $url, $options);
                    },
                    'deactivate' => function($url, $model) {
                        if ($model->status == 0) {
                            return '';
                        }
                        $options = [
                            'title' => Yii::t('begenk-auth', 'Activate'),
                            'aria-label' => Yii::t('begenk-auth', 'Activate'),
                            'data-confirm' => Yii::t('begenk-auth', 'Are you sure you want to ban this user?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="bi bi-ban"></span>', $url, $options);
                    }
                    ],
                ],
            ],
        ]);
        ?>
</div>
