<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use begenk\auth\components\Helper;

/* @var $this yii\web\View */
/* @var $model begenk\auth\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('begenk-auth', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$controllerId = $this->context->uniqueId . '/';
?>
<div class="user-view">
    <p>
        <?php
        if ($model->status == 0 && Helper::checkRoute($controllerId . 'activate')) {
            echo Html::a(Yii::t('begenk-auth', 'Activate'), ['activate', 'id' => $model->id], [
                'class' => 'btn btn-primary',
                'data' => [
                    'confirm' => Yii::t('begenk-auth', 'Are you sure you want to activate this user?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        <?php
        if ($model->status == 10 && Helper::checkRoute($controllerId . 'deactivate')) {
            echo Html::a(Yii::t('begenk-auth', 'Deactivate'), ['deactivate', 'id' => $model->id], [
                'class' => 'btn btn-primary',
                'data' => [
                    'confirm' => Yii::t('begenk-auth', 'Are you sure you want to ban this user?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        <?php
        if (Helper::checkRoute($controllerId . 'delete')) {
            echo Html::a(Yii::t('begenk-auth', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'full_name',
            'email:email',
            'created_at:date',
            [
                'attribute'=>'status',
                'value'=>function($model){
                    if($model->status == 10) 
                        return 'Active';
                    else 
                        return 'Not Active';
                }
            ],
        ],
    ])
    ?>

</div>
