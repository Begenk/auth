<?php

use yii\helpers\Html;

/* @var $this  yii\web\View */
/* @var $model begenk\auth\models\BizRule */

$this->title = Yii::t('begenk-auth', 'Create Rule');
$this->params['breadcrumbs'][] = ['label' => Yii::t('begenk-auth', 'Rules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">
    <?=
    $this->render('_form', [
        'model' => $model,
    ]);
    ?>

</div>
