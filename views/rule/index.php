<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this  yii\web\View */
/* @var $model begenk\auth\models\BizRule */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel begenk\auth\models\searchs\BizRule */

$this->title = Yii::t('begenk-auth', 'Rules');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-index">
    <p>
        <?= Html::a(Yii::t('begenk-auth', 'Create Rule'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('begenk-auth', 'Name'),
            ],
            ['class' => 'yii\grid\ActionColumn',],
        ],
    ]);
    ?>

</div>
