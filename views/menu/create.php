<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model begenk\auth\models\Menu */

$this->title = Yii::t('begenk-auth', 'Create Menu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('begenk-auth', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
