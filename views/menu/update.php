<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model begenk\auth\models\Menu */

$this->title = Yii::t('begenk-auth', 'Update Menu') . ': ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('begenk-auth', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('begenk-auth', 'Update');
?>
<div class="menu-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
