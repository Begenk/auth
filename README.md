begenk auth
===========
RBAC, 

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either add

```
"begenk/auth": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
"repositories": [
   {
        "type": "package",
        "package": {
            "name": "begenk/auth",
            "version": "1.0",
            "type": "yii2-extension",
            "source": {
                "url": "https://gitlab.com/begenk/auth.git",
                "type": "git",
                "reference": "master"
            },
            "require": {
                "yiisoft/yii2": "*"
            },
            "autoload": {
                "psr-4": {
                    "begenk\\auth\\": ""
                }
            }
        }
    }
]


For authentication is via an `auth.json` file
located in your `COMPOSER_HOME` or besides your `composer.json`.

```php
{
    "http-basic": {
        "gitlab.com": {
            "username": "Begenk",
            "password": "nganu123"
        }
    }
}
