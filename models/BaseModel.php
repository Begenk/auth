<?php

namespace begenk\auth\Models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use begenk\auth\components\LoggableBehavior;

class BaseModel extends ActiveRecord
{
    public function behaviors()
    {
        return [
            //'begenk\auth\components\LoggableBehavior'
        ];
    }

    

    public function beforeSave($insert)
    {
        // if ($this->isNewRecord) {
        //     if ($this->hasAttribute('created_at')) {
        //         $this->created_at = date('Y-m-d H:i:s');
        //     }

        //     if ($this->hasAttribute('created_by')) {
        //         $this->created_by = @Yii::$app->user->identity->id;
        //     }
        // } else {
        //     if ($this->hasAttribute('updated_at')) {
        //         $this->updated_at = date('Y-m-d H:i:s');
        //     }

        //     if ($this->hasAttribute('updated_by')) {
        //         $this->updated_by = @Yii::$app->user->identity->id;
        //     }
        // }
        return parent::beforeSave($insert);
    }
}
