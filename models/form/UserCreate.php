<?php
namespace begenk\auth\models\form;

use Yii;
use begenk\auth\models\User;
use yii\base\Model;

/**
 * Signup form
 */
class UserCreate extends Model
{
    public $full_name;
    public $username;
    public $email;
    public $password;
    public $retypePassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            [['username','full_name'], 'required'],
            ['username', 'unique', 'targetClass' => 'begenk\auth\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 3, 'max' => 255],
            ['full_name', 'string', 'min' => 3, 'max' => 100],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'begenk\auth\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['retypePassword', 'required'],
            [['retypePassword'], 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->full_name = $this->full_name;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->status = 0;
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
