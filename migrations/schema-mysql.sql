
/*Table structure for table `audit_trail` */

DROP TABLE IF EXISTS `audit_trail`;

CREATE TABLE `audit_trail` (
  `audit_trail_id` int(11) NOT NULL,
  `old_value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `new_value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `action` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `stamp` datetime NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ip_address` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `url_referer` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `browser` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `model_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `audit_trail` */

insert  into `audit_trail`(`audit_trail_id`,`old_value`,`new_value`,`action`,`model`,`field`,`stamp`,`user_id`,`user_name`,`ip_address`,`url_referer`,`browser`,`model_id`) values 
(0,NULL,NULL,'CREATE','begenk\\auth\\models\\Menu',NULL,'2017-10-17 12:51:46','1','superuser','127.0.0.1','/begenk-auth/menu/create','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36','14'),
(0,'','test','SET','begenk\\auth\\models\\Menu','name','2017-10-17 12:51:46','1','superuser','127.0.0.1','/begenk-auth/menu/create','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36','14'),
(0,'','2','SET','begenk\\auth\\models\\Menu','order','2017-10-17 12:51:46','1','superuser','127.0.0.1','/begenk-auth/menu/create','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36','14'),
(0,NULL,NULL,'DELETE','begenk\\auth\\models\\Menu',NULL,'2017-10-17 12:52:00','1','superuser','127.0.0.1','/begenk-auth/menu/delete?id=14','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36','14');

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_assignment` */

insert  into `auth_assignment`(`item_name`,`user_id`,`created_at`) values 
('superuser-role','1',1508151104),
('superuser-role','2',1508154649);

/*Table structure for table `auth_content` */

DROP TABLE IF EXISTS `auth_content`;

CREATE TABLE `auth_content` (
  `key` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `title` varchar(200) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `auth_content` */

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item` */

insert  into `auth_item`(`name`,`type`,`description`,`rule_name`,`data`,`created_at`,`updated_at`) values 
('/*',2,NULL,NULL,NULL,1508151422,1508151422),
('/begenk-auth/*',2,NULL,NULL,NULL,1508151422,1508151422),
('/begenk-auth/assignment/*',2,NULL,NULL,NULL,1508151415,1508151415),
('/begenk-auth/assignment/assign',2,NULL,NULL,NULL,1508151415,1508151415),
('/begenk-auth/assignment/index',2,NULL,NULL,NULL,1508151415,1508151415),
('/begenk-auth/assignment/revoke',2,NULL,NULL,NULL,1508151415,1508151415),
('/begenk-auth/assignment/view',2,NULL,NULL,NULL,1508151415,1508151415),
('/begenk-auth/content/*',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/content/create',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/content/delete',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/content/index',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/content/update',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/content/view',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/default/*',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/default/index',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/menu/*',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/menu/create',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/menu/delete',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/menu/index',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/menu/update',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/menu/view',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/permission/*',2,NULL,NULL,NULL,1508151417,1508151417),
('/begenk-auth/permission/assign',2,NULL,NULL,NULL,1508151417,1508151417),
('/begenk-auth/permission/create',2,NULL,NULL,NULL,1508151417,1508151417),
('/begenk-auth/permission/delete',2,NULL,NULL,NULL,1508151417,1508151417),
('/begenk-auth/permission/index',2,NULL,NULL,NULL,1508151416,1508151416),
('/begenk-auth/permission/remove',2,NULL,NULL,NULL,1508151417,1508151417),
('/begenk-auth/permission/update',2,NULL,NULL,NULL,1508151417,1508151417),
('/begenk-auth/permission/view',2,NULL,NULL,NULL,1508151417,1508151417),
('/begenk-auth/role/*',2,NULL,NULL,NULL,1508151419,1508151419),
('/begenk-auth/role/assign',2,NULL,NULL,NULL,1508151419,1508151419),
('/begenk-auth/role/create',2,NULL,NULL,NULL,1508151418,1508151418),
('/begenk-auth/role/delete',2,NULL,NULL,NULL,1508151419,1508151419),
('/begenk-auth/role/index',2,NULL,NULL,NULL,1508151417,1508151417),
('/begenk-auth/role/remove',2,NULL,NULL,NULL,1508151419,1508151419),
('/begenk-auth/role/update',2,NULL,NULL,NULL,1508151419,1508151419),
('/begenk-auth/role/view',2,NULL,NULL,NULL,1508151418,1508151418),
('/begenk-auth/route/*',2,NULL,NULL,NULL,1508151420,1508151420),
('/begenk-auth/route/assign',2,NULL,NULL,NULL,1508151419,1508151419),
('/begenk-auth/route/create',2,NULL,NULL,NULL,1508151419,1508151419),
('/begenk-auth/route/index',2,NULL,NULL,NULL,1508151419,1508151419),
('/begenk-auth/route/refresh',2,NULL,NULL,NULL,1508151420,1508151420),
('/begenk-auth/route/remove',2,NULL,NULL,NULL,1508151420,1508151420),
('/begenk-auth/rule/*',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/rule/create',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/rule/delete',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/rule/index',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/rule/update',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/rule/view',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/user/*',2,NULL,NULL,NULL,1508151422,1508151422),
('/begenk-auth/user/activate',2,NULL,NULL,NULL,1508151422,1508151422),
('/begenk-auth/user/change-password',2,NULL,NULL,NULL,1508151422,1508151422),
('/begenk-auth/user/create',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/user/deactivate',2,NULL,NULL,NULL,1508151422,1508151422),
('/begenk-auth/user/delete',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/user/index',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/user/login',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/user/logout',2,NULL,NULL,NULL,1508151421,1508151421),
('/begenk-auth/user/request-password-reset',2,NULL,NULL,NULL,1508151422,1508151422),
('/begenk-auth/user/reset-password',2,NULL,NULL,NULL,1508151422,1508151422),
('/begenk-auth/user/view',2,NULL,NULL,NULL,1508151421,1508151421),
('/redactor/*',2,NULL,NULL,NULL,1508151415,1508151415),
('assignment-permission',2,NULL,NULL,NULL,1508151579,1508151579),
('content-permission',2,NULL,NULL,NULL,1508151618,1508151618),
('menu-permission',2,NULL,NULL,NULL,1508151257,1508151257),
('permission-permission',2,NULL,NULL,NULL,1508151501,1508151501),
('role-permission',2,NULL,NULL,NULL,1508151462,1508151462),
('route-permission',2,NULL,NULL,NULL,1508215004,1508215004),
('rule-permission',2,NULL,NULL,NULL,1508151480,1508151480),
('superuser-role',1,NULL,NULL,NULL,1508151093,1508151093),
('user-permission',2,NULL,NULL,NULL,1508151521,1508151521);

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item_child` */

insert  into `auth_item_child`(`parent`,`child`) values 
('assignment-permission','/begenk-auth/assignment/*'),
('assignment-permission','/begenk-auth/assignment/assign'),
('assignment-permission','/begenk-auth/assignment/index'),
('assignment-permission','/begenk-auth/assignment/revoke'),
('assignment-permission','/begenk-auth/assignment/view'),
('content-permission','/begenk-auth/content/*'),
('content-permission','/begenk-auth/content/create'),
('content-permission','/begenk-auth/content/delete'),
('content-permission','/begenk-auth/content/index'),
('content-permission','/begenk-auth/content/update'),
('content-permission','/begenk-auth/content/view'),
('menu-permission','/begenk-auth/menu/*'),
('menu-permission','/begenk-auth/menu/create'),
('menu-permission','/begenk-auth/menu/delete'),
('menu-permission','/begenk-auth/menu/index'),
('menu-permission','/begenk-auth/menu/update'),
('menu-permission','/begenk-auth/menu/view'),
('permission-permission','/begenk-auth/permission/*'),
('permission-permission','/begenk-auth/permission/assign'),
('permission-permission','/begenk-auth/permission/create'),
('permission-permission','/begenk-auth/permission/delete'),
('permission-permission','/begenk-auth/permission/index'),
('permission-permission','/begenk-auth/permission/remove'),
('permission-permission','/begenk-auth/permission/update'),
('permission-permission','/begenk-auth/permission/view'),
('role-permission','/begenk-auth/role/*'),
('role-permission','/begenk-auth/role/assign'),
('role-permission','/begenk-auth/role/create'),
('role-permission','/begenk-auth/role/delete'),
('role-permission','/begenk-auth/role/index'),
('role-permission','/begenk-auth/role/remove'),
('role-permission','/begenk-auth/role/update'),
('role-permission','/begenk-auth/role/view'),
('route-permission','/begenk-auth/route/*'),
('route-permission','/begenk-auth/route/assign'),
('route-permission','/begenk-auth/route/create'),
('route-permission','/begenk-auth/route/index'),
('route-permission','/begenk-auth/route/refresh'),
('route-permission','/begenk-auth/route/remove'),
('rule-permission','/begenk-auth/rule/*'),
('rule-permission','/begenk-auth/rule/create'),
('rule-permission','/begenk-auth/rule/delete'),
('rule-permission','/begenk-auth/rule/index'),
('rule-permission','/begenk-auth/rule/update'),
('rule-permission','/begenk-auth/rule/view'),
('superuser-role','assignment-permission'),
('superuser-role','content-permission'),
('superuser-role','menu-permission'),
('superuser-role','permission-permission'),
('superuser-role','role-permission'),
('superuser-role','route-permission'),
('superuser-role','rule-permission'),
('superuser-role','user-permission'),
('user-permission','/begenk-auth/user/*'),
('user-permission','/begenk-auth/user/activate'),
('user-permission','/begenk-auth/user/change-password'),
('user-permission','/begenk-auth/user/create'),
('user-permission','/begenk-auth/user/deactivate'),
('user-permission','/begenk-auth/user/delete'),
('user-permission','/begenk-auth/user/index'),
('user-permission','/begenk-auth/user/login'),
('user-permission','/begenk-auth/user/logout'),
('user-permission','/begenk-auth/user/request-password-reset'),
('user-permission','/begenk-auth/user/reset-password'),
('user-permission','/begenk-auth/user/view');

/*Table structure for table `auth_menu` */

DROP TABLE IF EXISTS `auth_menu`;

CREATE TABLE `auth_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `auth_menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `auth_menu` */

insert  into `auth_menu`(`id`,`name`,`parent`,`route`,`order`,`data`) values 
(3,'Administrator',NULL,NULL,12,'users'),
(4,'User',3,'/begenk-auth/user/index',3,NULL),
(5,'Menu',3,'/begenk-auth/menu/index',4,NULL),
(6,'Route',13,'/begenk-auth/route/index',0,NULL),
(7,'Assignment',13,'/begenk-auth/assignment/index',3,NULL),
(9,'Permission',13,'/begenk-auth/permission/index',1,NULL),
(10,'Content',3,'/begenk-auth/content/index',NULL,NULL),
(12,'Role',13,'/begenk-auth/role/index',2,NULL),
(13,'Akses Kontrol',3,NULL,NULL,NULL);

/*Table structure for table `auth_notification` */

DROP TABLE IF EXISTS `auth_notification`;

CREATE TABLE `auth_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text,
  `level` varchar(10) NOT NULL DEFAULT 'info',
  `is_read` int(2) not null,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `auth_notification` */

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_rule` */

/*Table structure for table `auth_user` */

DROP TABLE IF EXISTS `auth_user`;

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_user` */

insert  into `auth_user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`) values 
(1,'superuser','SX0mCW9VV1y8W-KcdizVD5-AN-IY-Tq7','$2y$13$Sbd6LBchRX5Gzc7icGFQ4OQGFTKK/JS45ieVhNOxioiUqnJqEcc4K',NULL,'begenktrilupito@gmail.com',10,1503123223,1503123223),
(2,'begenk','wAs6YDxx0ZFT_X40dOjX9B18ZRFkXRRr','$2y$13$CpiyR/MTrrf.g8B4z6kRLuAMsiSr1tR1qTitiq5JAt2pAKIJLdl8a',NULL,'begenk.newbie@gmail.com',10,1508154611,1508154643);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
